package main;

import java.util.ArrayList;
import java.util.List;

public class Chat implements Mediator {
    private List<User> userList = new ArrayList<>();

    @Override
    public void sendMessage(String message, User sender) {
        for (User user : this.userList) {
            if (user != sender) {
                user.getSms(message);
            }
        }
    }

    @Override
    public void addUser(User user) {
        userList.add(user);
    }
}
