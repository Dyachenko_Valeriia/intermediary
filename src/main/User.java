package main;

public class User {
    private Mediator mediator;
    private String name;

    public User(Mediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }

    public void sendSms(String message) {//Отправить смс
        System.out.println(this.name + ": Отправил(а) смс: " + message);
        mediator.sendMessage(message, this);
    }

    public void getSms(String message) {//Получить смс
        System.out.println(this.name + ": Получил(а) смс: " + message);
    }
}
