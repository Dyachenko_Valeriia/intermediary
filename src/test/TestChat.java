package test;

import main.Chat;
import main.Mediator;
import main.User;
import org.junit.Test;

public class TestChat {
    @Test
    public void testChat() {
        Mediator mediator = new Chat();
        User user1 = new User(mediator, "Ксюша");
        User user2 = new User(mediator, "Лера");
        User user3 = new User(mediator, "Катя");
        User user4 = new User(mediator, "Вика");
        mediator.addUser(user1);
        mediator.addUser(user2);
        mediator.addUser(user3);
        mediator.addUser(user4);

        user1.sendSms("Привет! Как у вас дела?");
    }
}
